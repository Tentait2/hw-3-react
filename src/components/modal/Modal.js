import {useState} from "react";
import './Modal.scss'
import img from '../../resourses/img/cross.svg'
import PropTypes from "prop-types";
const Modal = (props) =>{
    const {id, header, modalText, backgroundColor, closeButton} = props.data

    const submitBucketModal = (e) =>{
        const {activeModalCardId, closeModal, handleBucketClick} = props
        closeModal({showModal:false})
        handleBucketClick(activeModalCardId)
    }

    const closeModal = (e) =>{
        const {closeModal} = props
        if (e.target.className === 'background' || e.target.className ==='close-modal' || e.target.value === 'closeModal'){
            closeModal({showModal:false})
        }
    }


        return (
            <div className={'background'} onClick={closeModal} >
                <div key={id} className={'modal'} style={{backgroundColor}}>
                    <h1>{header}</h1>
                    <p>{modalText}</p>
                    {closeButton ? <img className={'close-modal'} src={img} alt="#"/> : null}
                    <button className={'button-modal'} key={1}
                            onClick={submitBucketModal}
                    >Yes</button> <button className={'button-modal'} value={'closeModal'} key={2}>No</button>
                </div>
            </div>
        )

}

Modal.propTypes = {
    header: PropTypes.string,
    backgroundColor : PropTypes.string,
    modalText: PropTypes.string,
    closeButton: PropTypes.func,
    id: PropTypes.number,
};

export default Modal
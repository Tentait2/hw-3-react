import {Component} from "react";
import './Button.scss';
import PropTypes from "prop-types";
import ProductCard from "../productCard/ProductCard";

const Button = (props) => {
    const {data, backgroundColor, text, openModal, id} = props;

    return <button id={id} data-modal={data} className={'button-primary'} style={{backgroundColor}}
                   onClick={openModal}>{text}</button>;

}
Button.propTypes = {
    data: PropTypes.string,
    backgroundColor: PropTypes.string,
    text: PropTypes.string,
    openModal: PropTypes.func,
    id: PropTypes.number,
};

export default Button
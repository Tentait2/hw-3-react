import {useState, useEffect} from "react";

import './ProductCard.scss'
import Button from "../button/Button";

const ProductCard = (props) => {
    const {
        id,
        name,
        price,
        imgUrl,
        vendorCode,
        color,
        openModal,
        starItems,
        bucketItems,
        handleBucketClick,
        handleStarClick
    } = props



    const [starredStar, setStarredStar] = useState(false)
    const [starredBucket, setStarredBucket] = useState(false)


    useEffect(() => {
        if (starItems.includes(id)) {
            setStarredStar(true);
        } else {
            setStarredStar(false);
        }
    }, [id, starItems]);

    useEffect(() => {
        const isIdInBucket = bucketItems.includes(id);
        if (isIdInBucket) {
            setStarredBucket(true);
        } else {
            setStarredBucket(false);
        }
    }, [id, bucketItems]);


    const onStarClick = (id) => {
        setStarredStar(prevState => !prevState);
        handleStarClick(id);
    }

    const onBucketClick = (id) => {
        handleBucketClick(id);
    }
    return (
        <li className={'card-list-item'}>
            <img className={'list-img'} src={imgUrl} alt="item-img"/>
            <p className={'list-item-text'}>{name}</p>
            <p className={'list-item-text'}>{price} UAH</p>
            <p className={'list-item-text'}>{vendorCode}</p>
            <p className={'list-item-text'}> {color}</p>
            <p className={'item-favourite'} onClick={() => onStarClick(id)}>{starredStar ? '★' : '☆'}</p>
            {starredBucket ? null : <Button id={id} data={'modalID1'} backgroundColor={'grey'} text={`Add to cart`}
                                             openModal={openModal}/>}
            {starredBucket ? <Button id={id} data={'modalID2'} backgroundColor={'white'} text={"Remove item from bucket"}
                                                                                      handleBusterClick={onBucketClick} openModal={openModal}/>: null}
        </li>
    )
}


export default ProductCard
import './ProductList.scss'
import ProductCard from "../productCard/ProductCard";
import {useEffect, useState} from "react";
import Modal from "../modal/Modal";
import modalWindowDeclarations from "../../services/modalWindowDeclarations";

const ProductList = (props) => {
    const {productInfo, setStarItemsCount, setBucketItemsCount} = props

    const [starItems, setStarItems] = useState([]);
    const [bucketItems, setBucketItems] = useState([]);
    const [showModal, setShowModal] = useState(false);
    const [activeModal, setActiveModal] = useState({});
    const [activeModalCardId, setActiveModalCardId] = useState(null);


    useEffect(() => {
        let starItems = localStorage.getItem("starItems");
        if (starItems !== null) {
            setStarItemsCount(JSON.parse(starItems))
            setStarItems(JSON.parse(starItems));
        }
    }, []);

    useEffect(() => {
        let bucketItems = localStorage.getItem("bucketItems");
        if (bucketItems !== null) {
            setBucketItemsCount(JSON.parse(bucketItems))
            setBucketItems(JSON.parse(bucketItems));
        }
    }, []);


    const handleStarClick = (id) => {
        if (starItems.includes(id)) {
            const newStarItems = starItems.filter((item) => item !== id);
            setStarItems(newStarItems);
            setStarItemsCount(newStarItems)
            localStorage.setItem("starItems", JSON.stringify(newStarItems));
            props.getProductInfo(true,false)
        } else {
            const newStarItems = [...starItems, id];
            setStarItemsCount(newStarItems)
            setStarItems(newStarItems);
            localStorage.setItem("starItems", JSON.stringify(newStarItems));
        }
    };


    const handleBucketClick = (id) => {
        if (bucketItems.includes(+id)) {
            const newBucketItems = bucketItems.filter(item => item !== +id);
            setBucketItems(newBucketItems);
            props.setBucketItemsCount(newBucketItems)
            localStorage.setItem("bucketItems", JSON.stringify(newBucketItems));
            props.getProductInfo(false,true)
        } else {
            const newBucketItems = [...bucketItems, +id];
            props.setBucketItemsCount(newBucketItems)
            setBucketItems(newBucketItems);
            localStorage.setItem("bucketItems", JSON.stringify(newBucketItems));
        }
    };

    const getProducts = () => {
        const elements = productInfo.map((item) => {
            const {...itemProps} = item
            return (<ProductCard
                starItems={starItems}
                setStarItemsCount={setStarItemsCount}
                bucketItems={bucketItems}
                handleStarClick={handleStarClick}
                handleBucketClick={handleBucketClick}
                key={itemProps.id}
                openModal={openModal}
                {...itemProps}/>
            )
        })
        return elements
    }

    const getModalInfo = () => {
        return modalWindowDeclarations.map((item) => item);
    };

    const openModal = (e) => {
        const modalID = e.target.getAttribute("data-modal");
        const modalDeclaration = getModalInfo().find(
            (item) => item.modalId === modalID
        );
        setShowModal(true);
        setActiveModal(modalDeclaration);
        setActiveModalCardId(e.target.id);
    };

    const closeModal = () => {
        setShowModal(false);
    };

    return (
        <ul className="card-list">
            {getProducts()}
            {showModal ?
                <Modal
                    data={activeModal}
                    closeModal={closeModal}
                    activeModalCardId={activeModalCardId}
                    setSubmittedId={props.setSubmittedId}
                    handleBucketClick={handleBucketClick}
                /> : null}
        </ul>
    )

}

ProductList.defaultProps = {
    name: "Not in stock",
    price: 'sold out',
};


export default ProductList
const modalWindowDeclarations = [
    {
        modalId: 'modalID1',
        header: 'Do you want to add this product to backet?',
        modalText: 'Are you sure you want to do it?',
        backgroundColor: '#c7c7c7',
        closeButton : true
    },
    {
        modalId: 'modalID2',
        header: 'Do you want to delete this product?',
        modalText: 'Are you sure you want to do it?',
        backgroundColor: '#c7c7c7',
        closeButton : true,
    }
]

export default modalWindowDeclarations
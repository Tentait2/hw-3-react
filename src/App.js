import {useState, useEffect} from "react";
import {BrowserRouter as Router, Routes, Route} from 'react-router-dom'


import AppHeader from "./components/appHeader/AppHeader";
import Home from "./pages/home/Home";
import './App.scss';
import WishList from "./pages/wishlist/WishList";
import Bucket from "./pages/bucket/Bucket";
import GetProduct from "./services/GetProduct";


const App = () => {

    const [productInfo, setProductInfo] = useState([]);
    const [loading, setLoading] = useState(true);

    const [starItemsCount, setStarItemsCount] = useState([]);
    const [bucketItemsCount, setBucketItemsCount] = useState([]);

    const product = new GetProduct();
    const getProductInformation = async (wishList, bucketList) => {
        setLoading(true);
        const productInfo = await product.getProductInfo();
        setProductInfo([...productInfo]);
        if (wishList) {
            const filteredProducts = productInfo.filter(product => {
                return JSON.parse(localStorage.getItem('starItems')).includes(product.id);
            });
            setProductInfo([...filteredProducts]);
        }
        if (bucketList) {
            const filteredProducts = productInfo.filter(product => {
                return JSON.parse(localStorage.getItem('bucketItems')).includes(product.id);
            });
            setProductInfo([...filteredProducts]);
        }
        setLoading(false);
    };

    return (
        <Router>
            <AppHeader starItems={starItemsCount} bucketItems={bucketItemsCount}></AppHeader>
            <main>
                <Routes>
                    <Route path={'/'} element={<Home
                        setStarItemsCount={setStarItemsCount}
                        setBucketItemsCount={setBucketItemsCount}
                        getProductInformation={getProductInformation}
                        productInfo={productInfo}
                        loading={loading}
                    />}/>
                    <Route path={'/bucket'} element={<Bucket
                        setStarItemsCount={setStarItemsCount}
                        setBucketItemsCount={setBucketItemsCount}
                        getProductInformation={getProductInformation}
                        productInfo={productInfo}
                        loading={loading}
                    />}/>
                    <Route path={'/wishlist'} element={<WishList
                        setStarItemsCount={setStarItemsCount}
                        setBucketItemsCount={setBucketItemsCount}
                        getProductInformation={getProductInformation}
                        productInfo={productInfo}
                        loading={loading}
                    />}/>
                </Routes>
            </main>
        </Router>
    )
}


export default App;

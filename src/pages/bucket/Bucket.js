import {useEffect} from "react";
import ProductList from "../../components/productList/ProductList";

const Bucket = (props) => {
    useEffect(() => {
        props.getProductInformation(false,true)
    }, [])

    return (
        <>
            {props.loading ? (<div>Loading...</div>) : (
                <ProductList
                    getProductInfo={props.getProductInformation}
                    productInfo={props.productInfo}
                    setStarItemsCount={props.setStarItemsCount}
                    setBucketItemsCount={props.setBucketItemsCount}
                />)}
        </>

    )
}

export default Bucket
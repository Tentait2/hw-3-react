import {useEffect} from "react";
import ProductList from "../../components/productList/ProductList";

const WishList = (props) => {
    useEffect(() => {
        props.getProductInformation(true,false)
    }, [])

    return (
        <>
            {props.loading ? (<div>Loading...</div>) : (
                <ProductList
                    getProductInfo={props.getProductInformation}
                    productInfo={props.productInfo}
                    setStarItemsCount={props.setStarItemsCount}
                    setBucketItemsCount={props.setBucketItemsCount}
                />)}
        </>

    )
}

export default WishList
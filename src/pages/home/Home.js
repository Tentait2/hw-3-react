import {useEffect} from "react";
import ProductList from "../../components/productList/ProductList";

const Home = (props) => {
    useEffect(() => {
        props.getProductInformation(false,false)
    }, [])

    return (
        <>
            {props.loading ? (<div>Loading...</div>) : (
                <ProductList
                    productInfo={props.productInfo}
                    setStarItemsCount={props.setStarItemsCount}
                    setBucketItemsCount={props.setBucketItemsCount}
                />)}
        </>

    )
}

export default Home